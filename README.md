# data
Basic datasource template 

## prerequisites
Install homebrew from https://brew.sh/
```
$ brew tap AdoptOpenJDK/openjdk
$ brew install --cask adoptopenjdk15
$ brew install maven
```

## build
```
$ mvn clean install --update-snapshots 
```

## run
```
$ mvn compile exec:java -Dexec.mainClass="io.gitlab.vahidshirvani.App"
```
