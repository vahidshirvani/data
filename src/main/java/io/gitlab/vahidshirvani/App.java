package io.gitlab.vahidshirvani;

import com.mongodb.MongoClient;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class App
{
    public static void main( String[] args )
    {
        try (Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/datatest", "root", "")) {
            System.out.println("connected successfully!");
        }
        catch (SQLException ex) {
            System.out.println("connection failed!");
        }

        try (MongoClient connection = new MongoClient("localhost", 27017)) {
            System.out.println("connected successfully!");
        }
        catch (Exception ex) {
            System.out.println("connection failed!");
        }
    }
}
